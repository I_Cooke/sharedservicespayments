<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN"
	"http://www.w3.org/TR/html4/strict.dtd">
<%@page import="com.bintech.webservices.payment.ProcessCredCard"%>
<%@page import="java.util.Hashtable"%>
<%
	Hashtable<String, String> remoteHostsHT = new Hashtable<String, String>();
	remoteHostsHT.put("192.168.50.42", "DEV");
	remoteHostsHT.put("10.20.10.140", "Irina's PC");
	remoteHostsHT.put("192.168.50.41", "CA");
	remoteHostsHT.put("10.30.100.14", "Production Portal");
	remoteHostsHT.put("172.19.11.2", "Calvin");
	remoteHostsHT.put("10.30.101.10", "Production Hobbes");
	remoteHostsHT.put("192.168.50.38", "Web Proxy 192");
	remoteHostsHT.put("0:0:0:0:0:0:0:1", "Web Proxy Local 0:0");
	remoteHostsHT.put("127.0.0.1", "Web Proxy Local 127");
	java.util.Set<String> incomingIps = remoteHostsHT.keySet();
	String remoteHost = remoteHostsHT.get(request.getRemoteHost());
	
	java.util.Properties props = new java.util.Properties();
	String remoteHostColor="RED";
	String validity = "Invalid";
	if (incomingIps.contains(request.getRemoteHost())) {		
		validity = " Valid ";
		if(remoteHost.startsWith("Production"))
			remoteHostColor="RED";
		 else remoteHostColor="green";
	} else{
		remoteHostColor="orange";
		
	}
%>

<html lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<title>Bankers Insurance Group</title>



<style type="text/css">
.menulist, .menulist ul {
	margin: 0;
	padding: 0;
	list-style: none;
	color: #ffffff;
	font: 8.5pt 'Times New Roman';
	text-align: left;
	text-decoration: none;
}

.menulist ul {
	display: none;
	position: absolute;
	left: 10px;
	width: 150;
}

.menulist ul ul {
	top: 7px;
	margin-top: 0;
	left: 148px;
}

.menulist li {
	float: right;
	position: relative;
	background: #fb7256;
	border: 0px solid;
	padding: 0px;
}

.menulist ul li {
	float: none;
	margin: 0;
	border-top: 1px solid #ae9e85;
}

.menulist ul>li:last-child {
	margin-bottom: 1px; /* Mozilla fix */
}

.menulist a {
	display: block;
	padding: 5;
	color: #ffffff;
	text-decoration: none;
}

a.s {
	display: block;
	padding: 5 0 0 0;
}

.menulist a:hover, .menulist a.highlighted:hover, .menulist a:focus {
	color: #ffffff;
	background-color: #f05734;
}

.menulist a.highlighted {
	color: #ffffff;
	background-color: #f05734;
}

.menulist a .subind {
	display: none;
}

.menulist ul a .subind {
	display: block;
	float: right;
}

.subind {
	font: bold .6em arial;
	padding-top: 3px;
}
/* 'Escaped Comment' hack for horizontal menubar width in IE5/Mac */
.menulist a {
	float: left;
}

.menulist ul a {
	float: none;
}
/* \*/
.menulist a {
	float: none;
}
/* */
*:first-child+html .menulist ul li {
	float: left;
	width: 100%;
}

* html .menulist ul li {
	float: left;
	height: 1%;
}

* html .menulist ul a {
	height: 1%;
}
</style>


<style type="text/css">
#MB_loading {
	font-size: 13px;
}

.gb1 {
	background-color: #ffffff;
}

.gb2 {
	background-color: #fdc8bd;
}

.gb3 {
	background-color: #fb7256;
}

.tb {
	border-width: 1px;
	border-color: #000000;
	border-style: solid;
	padding-left: 3px;
}

.prnHdr {
	border-width: 1px 1px 0px 1px;
	border-color: #000000;
	border-style: solid;
	padding-left: 3px;
	font: bold 10pt 'Arial';
	background-color: #fb7256;
	color: #ffffff;
}

.plr {
	padding: 0px 7x 0px 7px;
}

.br1 {
	border-right: solid #000000 1px;
}

.tbr {
	border-width: 0px 1px 0px 0px;
	border-right-color: #000000;
	border-style: solid;
	padding: 3px;
}

table, div {
	border-collapse: collapse;
}

.prn1 {
	color: #000000;
	font: normal 8pt 'Arial';
}

.prn2 {
	color: #000000;
	font: normal 10pt 'Arial';
}

.prnOCR {
	color: #000000;
	font: bold 10pt 'Courier New'
}

.xsell {
	padding: 2px;
	font: normal 7.5pt 'Verdana';
	width: 80%;
	height: 50;
	background-color: #000000;
	color: #ffffff;
}

.logo {
	margin: 10px;
	border: 0px;
	height: 85;
	width: 200;
	background: url(/Company/Bankers/Images/PrintLogo.gif);
}

.BIGlogo {
	margin: 10px;
	border: 0px;
	height: 85;
	width: 200;
	background: url(/Company/Bankers/Images/PrintLogo.gif);
}

form {
	margin: 0px;
}

select, select, input, textarea, .tx1, #divHelp {
	font: normal 7.5pt 'Verdana';
	color: #000000;
}

.tmnu {
	vertical-align: top;
	font: normal 8.5pt 'Times New Roman';
	color: #ffffff
}

.u {
	cursor: pointer;
	text-decoration: underline;
	font-weight: normal;
}

.lmnu {
	height: 20px;
	font: bold 7.5pt 'Verdana';
	color: #ffffff;
	text-decoration: underline;
}

.rmnu, .rmnu_u {
	cursor: pointer;
	border-bottom: solid 1px #000000;
	width: 90%;
	text-align: center;
	font: normal 7.5pt 'Verdana';
	color: #000000;
}

.tx2 {
	font: bold 10.5pt 'Times New Roman';
	color: #9b6028
}

.tx2i {
	font: italic bold 7.5pt 'Verdana';
	color: #000000
}

.tx2ie {
	font: italic bold 10.5pt 'Times New Roman';
	color: #ff0000
}

.btn {
	height: 28;
	width: 140;
	background: url('/Company/Bankers/Images/btnr.gif');
	text-decoration: none;
	color: #ffffff;
}

.btn2 {
	height: 28;
	width: 140;
	background: url('/Company/Bankers/Images/btnl.gif');
	text-decoration: none;
	color: #ffffff;
}

.btntiny {
	background: url('/Company/Bankers/Images/btnl.gif');
	text-decoration: none;
	color: #ffffff;
}

.e {
	color: #ff0000;
}

.errorOn {
	background-color: #99ccff;
}

.d {
	color: #c0c0c0;
}

hr, .hr {
	border: solid 1px #000000;
}

.hr1 {
	height: 1px
}

.smaller {
	font-size: .8em;
}

.r {
	text-align: right;
}

.l {
	text-align: left;
}

.c {
	text-align: center;
}

.j {
	text-align: justify;
}

.b {
	font-weight: bold;
}

.i {
	font-style: italic;
}

.vat {
	vertical-align: top;
}

#bodytable {
	height: 100%;
	width: 822px;
	background-color: #ffffff;
	background: url('/Company/Bankers/Images/bodbg.jpg');
}

#content {
	width: 625px;
	padding-left: 20px;
	padding-top: 5px;
}

#menuRight {
	height: 100%;
	width: 154px;
	border-left: solid #000000 1px;
}

#menuLeft {
	height: 100%;
	width: 154px;
}

#copyright {
	padding-top: 20px;
	padding-bottom: 0px;
	text-align: center;
	vertical-align: bottom;
}

body {
	background-color: #9b6028;
	margin: -4 0 0 0;
}

.icon, #icon {
	cursor: pointer;
}

select {
	width: 200px;
}

@media print {
	#bodytable {
		height: 0px;
		width: 800;
		background: url('/Company/Bankers/Images/');
	}
	#content {
		width: 100%;
		padding: 0
	}
	#leftmenu, #icon, .icon, #pagetop, #copyright, #menu, #gradient, .btn {
		display: none;
	}
	#menuRight {
		height: 100%;
		width: 154;
		border-left: 0px;
	}
	.logo {
		background: url(/Company/Bankers/Images/PrintLogoBW.gif);
	}
	.BIGlogo {
		margin: 10px;
		border: 0px;
		height: 85;
		width: 200;
		background: url(/Company/Bankers/Images/PrintLogoBW.gif);
	}
	.page {
		vertical-align: top;
		height: 1004;
	}
	.prnHdr {
		font: bold 10pt 'Arial';
		background-color: #000000;
		color: #ffffff;
	}
	.tbr {
		border-width: 0px 1px 0px 0px;
		border-right-color: #000000;
		padding: 0px;
		padding-left: 3px;
		padding-right: 3px;
	}
	.tb {
		border-color: #000000;
		padding: 0px;
		padding-left: 3px;
		padding-right: 3px;
	}
	.tx2 {
		color: #000000;
	}
	body {
		background-color: #ffffff;
	}
	hr, .hr {
		width: 100%;
		border: solid 2px #000000;
	}
	hr.prn1 {
		border: solid 1px #000000;
	}
	p.br {
		height: 1px;
	}
}
</style>

<style>
body {
	background: #f4f0e6 url(layout_pattern.png);
	margin: 0px;
	padding: 0px;
	color: #333;
	font-size: 9.5pt;
	font-family: Verdana;
}
</style>
</head>
<body>


	<table align="center" id="pagetop" style="width: 960;" cellpadding="0"
		cellspacing="0">
		<tr>
			<td colspan="2" style="vertical-align: top;"><img border="0"
				src="header2.jpg"></td>
		</tr>
	</table>
	<table align="center" style="width: 80%; height: 100%;"
		cellpadding="10" cellspacing="0">
		<tr>
			<td
				style="vertical-align: top; background-color: #ffffff; width: 100%;">
				<table width="100%" cellpadding="0" cellspacing="0">
					<tr>
						<td colspan="2" class="tx2">Bankers Insurance<br> <br></td>
					</tr>
					<tr>
						<td colspan="2" class="tx1">Web Proxy</td>
					</tr>
					<tr>
						<td class="tx1 b">Request from:<br><%
						out.print("<SPAN STYLE='color: "+remoteHostColor+"; font-size: 10pt'> "+validity+" request from: " + remoteHost + "</SPAN><br>remoteHostMsg");
							out.println("request.getRemoteAddr()=" + request.getRemoteAddr() + "<br>");
							out.println("request.getRequestURI()=" + request.getRequestURI() + "<br>");
							out.println("request.getRemoteHost=" + request.getRemoteHost() + "<br>");
							out.println("request.getRemoteUser()=" + request.getRemoteUser() + "<br>");
							out.println("request.getQueryString()=" + request.getQueryString() + "<br>");
						%>
						</td>

					</tr>
					<tr>
						<td class="tx1 b"></td>
						<td class="tx1"><a href="/Consumer">If you are a
								policyholder click here.</a></td>
					</tr>
					<tr>
						<td colspan="2" class="tx2"><br></td>
					</tr>
					<tr>
						<td colspan="2" class="tx2">Need Help?<br> <br></td>
					</tr>
					<tr>
						<td colspan="2" class="tx1">For password assistance, please
							contact your dedicated <a
							href="http://pages.sfmcmail.bankersinsurance.com/BUI_TSM/"
							target="_new">Territory Sales Manager</a> or the following:<br>
							<br> Administrative (master) credentials: 1.800.627.0000,
							ext. 4418 or <a href="mailto:contracting@bankersinsurance.com"
							target="_new">contracting@bankersinsurance.com</a><br>
							Producer/CSR/Staff User IDs:<br>
							<ul>
								<li>Personal Lines 1.800.627.0000, ext. 4703 or <a
									href="mailto:homeowners@bankersinsurance.com" target="_new">homeowners@bankersinsurance.com</a><br></li>
								<li>Commercial Lines 1.800.627.0000, ext. 4035 or <a
									href="mailto:commercial@bankersinsurance.com" target="_new">commercial@bankersinsurance.com</a><br></li>
								<li>Flood Lines 1.800.627.0000, ext. 4704 or <a
									href="mailto:flood@bankersinsurance.com" target="_new">flood@bankersinsurance.com</a><br></li>
							</ul>
						</td>
					</tr>
				</table> <input type="hidden" name="Logon" id="Logon" value="">
			</td>
		</tr>
	</table>

	<table align="center" style="width: 960;" cellpadding="0"
		cellspacing="0">
		<tr>
			<td align="center" valign="bottom" style="padding-bottom: 10px;"
				class="tx1">
				<div id="copyright">
					<span class="tx1">A Bankers Financial Corporation Company |
						&copy; 1976-2010</span><br> <span class="tx1">800-627-0000</span> <br>
					<br> <span class="tx1 i" style="color: gray;">This site
						is best viewed with IE7, IE8 and FireFox 3.5 and higher</span>
				</div>
			</td>
			<td align="center" valign="bottom" style="padding-bottom: 30px;"
				class="tx1 c"><img style="border: none;"
				title="This Web site is secured with a GoDaddy.com Web Server Certificate. Transactions on the site are protected with up to 256-bit Secure Sockets Layer encryption."
				src="godaddyssl.png" id="sslImg"></a></td>
		</tr>
	</table>
</body>

</html>
