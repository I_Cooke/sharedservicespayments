package com.bintech.webservices.payment;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.net.ftp.FTPClient;

import net.authorize.Environment;
import net.authorize.api.contract.v1.ArrayOfLineItem;
import net.authorize.api.contract.v1.BankAccountTypeEnum;
import net.authorize.api.contract.v1.CreateTransactionRequest;
import net.authorize.api.contract.v1.CreateTransactionResponse;
import net.authorize.api.contract.v1.CustomerDataType;
import net.authorize.api.contract.v1.EcheckTypeEnum;
import net.authorize.api.contract.v1.LineItemType;
import net.authorize.api.contract.v1.MerchantAuthenticationType;
import net.authorize.api.contract.v1.MessageTypeEnum;
import net.authorize.api.contract.v1.MessagesType;
import net.authorize.api.contract.v1.MessagesType.Message;
import net.authorize.api.contract.v1.PaymentType;
import net.authorize.api.contract.v1.TransactionRequestType;
import net.authorize.api.contract.v1.TransactionResponse;
import net.authorize.api.contract.v1.TransactionTypeEnum;
import net.authorize.api.controller.CreateTransactionController;
import net.authorize.api.controller.base.ApiOperationBase;
import test.bintech.webservices.payment.TestPayments;
/**
 * @author icooke
 * @Created On Feb 18, 2016
 * @Modified by icooke Ticket #82267
 * @Modified by icooke On May 9, 2016 Ticket #82360
 */
public class ProcessECheck{

	//private static Log logger = LogFactory.getLog(PaymentService.class);
	public static String processECheckRequest(HttpServletRequest request, HttpServletResponse response
			, Properties props){
		String companyNum = request.getParameter("companyNum");
		String product = request.getParameter("product");
		String itemId = request.getParameter("policyNumber");

		String accountType = request.getParameter("accountType");
		String accountNumber = request.getParameter("accountNumber");
		String bankName = request.getParameter("bankName");
		String routingNumber = request.getParameter("routingNumber");
		String nameOnAccount = request.getParameter("nameOnAccount");
		String subline = request.getParameter("subline");
		String amount = request.getParameter("x_amount") != null ? request.getParameter("x_amount") : "";
		String returnMessage = "";
		try {
			java.math.BigDecimal amountBD = new java.math.BigDecimal(amount);			
			returnMessage = ProcessECheck.processECheck(companyNum
					, product, subline, itemId, props, accountType
					, accountNumber, bankName, routingNumber, nameOnAccount, amountBD);
		} catch (Exception e) {
			System.out.println("Invalid payment Request.");
			e.printStackTrace();
		}
		return returnMessage;
	}
	public static String processECheck(String companyNum, String product
			, String subline,String itemId,
			Properties props, String accountType, String accountNumber, String bankName,
			String routingNumber, String nameOnAccount, BigDecimal amount) {

		// Common code to set for all requests

		String paymentInfo = "Company:"+companyNum + " Product:"+product+" Subline: "+subline;
		Environment environment = Environment.SANDBOX;
		MerchantAuthenticationType merchantAuthenticationType = new MerchantAuthenticationType();

		String interfaceId = props.getProperty(companyNum + "ECheckInterfaceId");
		String interfaceKey = props.getProperty(companyNum + "ECheckInterfaceKey");
		if (interfaceId == null && interfaceKey == null) {
			String errMessage = "Error: System data is missing to process this transaction.";
			System.out.println("\n" + errMessage + " (interfaceId, interfaceKey) for company " + companyNum);
			return errMessage;
		}
		merchantAuthenticationType.setName(interfaceId);
		merchantAuthenticationType.setTransactionKey(interfaceKey);

		boolean isProdEnvironment = props!=null 
				&& props.getProperty("System")!=null
				&&props.getProperty("System").equals("PROD");
		if (isProdEnvironment) {			
			environment = Environment.PRODUCTION;
		}
		System.out.println("\n E-Check Environment BaseUrl:" + environment.getBaseUrl() + "\n");

		ApiOperationBase.setEnvironment(environment);
		ApiOperationBase.setMerchantAuthentication(merchantAuthenticationType);

		PaymentType paymentType = new PaymentType();
		net.authorize.api.contract.v1.BankAccountType bankAccountType = new net.authorize.api.contract.v1.BankAccountType();

		bankAccountType.setAccountNumber(accountNumber);// "123456");

		System.out.println(accountType);
		if (accountType != null)
			if (accountType.equals("BusinessChecking"))
				bankAccountType.setAccountType(BankAccountTypeEnum.BUSINESS_CHECKING);
			else if (accountType.equals("Checking"))
				bankAccountType.setAccountType(BankAccountTypeEnum.CHECKING);
			else if (accountType.equals("Savings"))
				bankAccountType.setAccountType(BankAccountTypeEnum.SAVINGS);
		bankAccountType.setBankName(bankName);
		bankAccountType.setCheckNumber("123");
		bankAccountType.setEcheckType(EcheckTypeEnum.WEB);
		bankAccountType.setNameOnAccount(nameOnAccount);
		bankAccountType.setRoutingNumber(routingNumber);// "063100277");

		paymentType.setBankAccount(bankAccountType);

		TransactionRequestType txnRequest = new TransactionRequestType();
		CustomerDataType customerDataType = new CustomerDataType();
		customerDataType.setId(itemId);
		ArrayOfLineItem arr = new ArrayOfLineItem();
		List<LineItemType> lis = arr.getLineItem();
		lis = new ArrayList<LineItemType>();
		LineItemType li = new LineItemType();
		li.setDescription(paymentInfo);
		lis.add(li);
		txnRequest.setLineItems(arr);
		txnRequest.setCustomer(customerDataType);
		txnRequest.setTransactionType(TransactionTypeEnum.AUTH_CAPTURE_TRANSACTION.value());
		txnRequest.setPayment(paymentType);
		// txnRequest.setCustomer(customer);
		txnRequest.setAmount(amount);

		// Make the API Request
		CreateTransactionRequest apiRequest = new CreateTransactionRequest();
		apiRequest.setTransactionRequest(txnRequest);
		CreateTransactionController controller = new CreateTransactionController(apiRequest);

		String returnMessage =  ProcessECheck.processECheckTransactionResponse(controller
				, "E-Check: ",itemId, isProdEnvironment,  props , nameOnAccount, amount
				, companyNum, paymentInfo);				
				
		returnMessage = returnMessage!=null?returnMessage:"";
		boolean isTransactionSuccessful = returnMessage.contains("Transaction ID:");
		try {
			String fileName = buildFileName(returnMessage, isProdEnvironment, isTransactionSuccessful,
					itemId, companyNum);
			if(props !=null) sendFTP(fileName, returnMessage,  props );
		} catch (Exception e) {
			e.printStackTrace();
		}

		return returnMessage;
	}

	public static String processECheckTransactionResponse(CreateTransactionController controller, String returnMessage,
			String policyNumber, boolean isProdEnvironment, Properties layout, String payeeName,
			BigDecimal amountBD, String companyNumber, String paymentInfo) {

		boolean isTransactionSuccessful = false;
		StringBuffer resultMessageSB = new StringBuffer();

		if (isProdEnvironment) {
			resultMessageSB.append("This transaction ");
		} else {
			resultMessageSB.append(
					"Testing. Attention: This is a test transaction, it should not be processed!!!\r\n This transaction ");
		}
		try {
			controller.execute();
			CreateTransactionResponse response = controller.getApiResponse();
			String amount = NumberFormat.getCurrencyInstance().format(amountBD);
			List<Message> messages = null;
			if (response != null) {
				MessagesType messagesType = response.getMessages();

				if (messagesType != null) {
					messages = messagesType.getMessage();

					if (messagesType.getResultCode() == MessageTypeEnum.OK
							&& response.getTransactionResponse().getResponseCode().equals("1")) {// 2
						isTransactionSuccessful = true;
						String dt = new SimpleDateFormat("MM/dd/yyyy").format(new java.util.Date());

						returnMessage = "Payment Date: " + dt + "  Payment Amount: " + amount + "  Transaction ID: "
								+ response.getTransactionResponse().getTransId();
						resultMessageSB.append(" with " + returnMessage + " was processed successfully. \r\n ");
						
					} else {
						resultMessageSB.insert(0, "Error: ");
						resultMessageSB.append(" failed: ");
						returnMessage = "Error: ";
						try {
							if (messages != null && messagesType.getMessage().size() > 0) {
								for (Message message : messages) {
									if (message != null) {
										String errMsg = message.getText() != null ? message.getText() : "";

										resultMessageSB.append("\r\n Code:" + message.getCode() + " Message:" + errMsg);
										if (errMsg.equals(PaymentService.AUTH_ERR)) {
											returnMessage += PaymentService.AUTH_ERR_DISPL;

										}
									}
								}
							}
							List<TransactionResponse.Errors.Error> errors = response.getTransactionResponse() != null
									&& response.getTransactionResponse().getErrors() != null
											? response.getTransactionResponse().getErrors().getError() : null;
							if (errors != null) {
								for (TransactionResponse.Errors.Error error : errors) {
									if (error != null && error.getErrorText() != null) {
										String errText = error.getErrorText();
										resultMessageSB.append(
												"\r\n Error Code:" + error.getErrorCode() + "\r\n Error: " + errText);
										if (errText.equals(PaymentService.ACC_ERR)) {
											returnMessage += errText + ".";
										} else if (errText.equals(PaymentService.ROUT_ERR)) {
											returnMessage += PaymentService.ROUT_ERR_DISPL;
										} else if (errText.equals(PaymentService.DUPL_ERR)) {
											returnMessage += PaymentService.DUPL_ERR;
										} else if (errText.equals(PaymentService.DECLINED_ERR)) {
											returnMessage += PaymentService.DECLINED_ERR +"\n\r Please try to change payment amount";
										} else if (errText.equals(PaymentService.NOCHECKS_ERR)) {
											returnMessage += PaymentService.NOCHECKS_ERR_DISPL;
										} else if (errText.equals(PaymentService.MAXEXCEEDED_ERR)) {
											returnMessage += PaymentService.MAXEXCEEDED_ERR +"\n\r Please try to change payment amount";
										} else {
											returnMessage += PaymentService.GENERIC_ERR_DISPL;
										}
									} else {
										returnMessage += PaymentService.GENERIC_ERR_DISPL;
									}
								}
							}
						} catch (Exception e) {
							e.printStackTrace();
						}
					}
				}
			} else {
				returnMessage = "Error: " + PaymentService.GENERIC_ERR_DISPL;
				resultMessageSB.insert(0, "Error: SYSTEM ERROR \r\n");
				resultMessageSB.append(" failed: ");
				List<String> results = controller.getResults();
				if (results != null && results.size() > 0) {
					for (String result : results) {
						System.out.println("\r\nresult" + result);
						resultMessageSB.append(" failed: " + result);
					}
				}
			}
			if (returnMessage != null && returnMessage.equals("Error: "))
				returnMessage += PaymentService.GENERIC_ERR_DISPL;
			resultMessageSB.append(" \r\n Policy: " + policyNumber + " \r\n "+paymentInfo
					+ "   \r\n Amount: " + amount + "   \r\n Name on a check: " + payeeName);
		} catch (Exception e) {
			resultMessageSB.insert(0, "Error: SYSTEM ERROR \r\n");
			resultMessageSB.append(" failed: ");
			returnMessage = "Error: " + returnMessage;
			e.printStackTrace();
		}

		try {
			String fileName = buildFileName(resultMessageSB.toString(), isProdEnvironment
					, isTransactionSuccessful,
					policyNumber, companyNumber);
			sendFTP(fileName, resultMessageSB.toString(), layout);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return returnMessage;
	}

	public static String buildFileName(String content, boolean prodEnvironment
			, boolean isTransactionSuccessful,
			String policyNumber, String companyNum) {

		StringBuffer fileNameBS = new StringBuffer(companyNum + "_" + policyNumber + "_");

		if (isTransactionSuccessful) {
			if (prodEnvironment)
				fileNameBS.append("processedEcheck_");
			else
				fileNameBS.append("testEcheck_");
		} else {
			if (prodEnvironment)
				fileNameBS.append("failedEcheck_");
			else
				fileNameBS.append("testFailedEcheck_");
		}
		String dt = new SimpleDateFormat("ddMMyy_HHmmss").format(new java.util.Date());
		fileNameBS.append(dt + ".txt");

		return fileNameBS.toString();
	}

	public static boolean sendFTP(String fileName, String content, Properties layout) {

		String ftpServer = "BHPSHARESWH01";
		int ftpPort = 21;
		String ftpUser = "echeck";
		String ftpPwd = "Acc0unting@1";

		if (layout != null) {

			String ftpPortStr = layout.getProperty("eCheckFtpPort");
			if (ftpPortStr != null && ftpPortStr.length() > 0) {
				ftpServer = layout.getProperty("eCheckFtpServer");
				ftpPort = Integer.parseInt(ftpPortStr);
				ftpUser = layout.getProperty("eCheckFtpUser");
				ftpPwd = layout.getProperty("eCheckFtpPwd");
			}

		}

		FTPClient ftpClient = new FTPClient();
		try {

			ftpClient.connect(ftpServer, ftpPort);
			ftpClient.login(ftpUser, ftpPwd);
			ftpClient.enterLocalPassiveMode();

			ftpClient.setFileType(FTPClient.BINARY_FILE_TYPE);

			String directoryName = PaymentService.DIRECTORY_NAME;
			String remoteFileName = directoryName + fileName;

			InputStream inputStream = new ByteArrayInputStream(content.getBytes("UTF-8"));
			System.out.println("Start uploading " + ftpServer + "/" + fileName + " file");
			boolean done = ftpClient.storeFile(remoteFileName, inputStream);
			inputStream.close();
			if (done) {
				System.out.println(remoteFileName + " is uploaded successfully.");
				return true;
			} else
				return false;

		} catch (IOException ex) {
			System.out.println("Error: " + ex.getMessage());
			ex.printStackTrace();
			return false;
		} finally {
			try {
				if (ftpClient.isConnected()) {
					ftpClient.logout();
					ftpClient.disconnect();
				}
			} catch (IOException ex) {
				ex.printStackTrace();
			}
		}
	}
}
