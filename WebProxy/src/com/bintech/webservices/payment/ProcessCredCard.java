package com.bintech.webservices.payment;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStreamReader;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.net.URL;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Properties;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLSession;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import net.authorize.Environment;
import net.authorize.api.contract.v1.CreateTransactionRequest;
import net.authorize.api.contract.v1.CreateTransactionResponse;
import net.authorize.api.contract.v1.CreditCardType;
import net.authorize.api.contract.v1.CustomerAddressType;
import net.authorize.api.contract.v1.CustomerDataType;
import net.authorize.api.contract.v1.MerchantAuthenticationType;
import net.authorize.api.contract.v1.MessagesType;
import net.authorize.api.contract.v1.MessagesType.Message;
import net.authorize.api.contract.v1.PaymentType;
import net.authorize.api.contract.v1.TransactionRequestType;
import net.authorize.api.contract.v1.TransactionResponse;
import net.authorize.api.contract.v1.TransactionTypeEnum;
import net.authorize.api.controller.CreateTransactionController;
import net.authorize.api.controller.base.ApiOperationBase;
import test.bintech.webservices.payment.TestPayments;

/**
 * @author icooke
 * @Created On Feb 18, 2016
 * @Modified by icooke Ticket #82267
 * @Modified by icooke On May 9, 2016 Ticket #82360
 */
public class ProcessCredCard {

	// private static Log logger = LogFactory.getLog(PaymentService.class);

	public static String processCredCardRequest(HttpServletRequest request, HttpServletResponse response,
			Properties props) {
		
		String passLogin = "x_login=" + request.getParameter("x_login") + "&";
		String passTranKey = "x_tran_key=" + request.getParameter("x_tran_key") + "&";
		String passVersion = "x_version=" + request.getParameter("x_version") + "&";
		String passTestReq = "x_test_request=" +  request.getParameter("x_test_request") + "&";//props.getProperty("System") + "&";
		String passDupWindow = "x_duplicate_window=" +  request.getParameter("x_duplicate_window") + "&";
		String passFirstName = "x_first_name=" + request.getParameter("x_first_name") + "&";
		String passLastName = "x_last_name=" + request.getParameter("x_last_name") + "&";
		String passAddress = "x_address=" + request.getParameter("x_address") + "&";
		String passCity = "x_city=" + request.getParameter("x_city") + "&";
		String passState = "x_state=" + request.getParameter("x_state") + "&";
		String passZip = "x_zip=" + request.getParameter("x_zip") + "&";
		String passCustId = "x_cust_id=" + request.getParameter("x_cust_id") + "&";
		String passAmount = "x_amount=" +  request.getParameter("x_amount") + "&";
		String passTranType = "x_type=" + request.getParameter("x_type") + "&";
		String passCardNum = "x_card_num=" + request.getParameter("x_card_num") + "&";
		String passExpDate = "x_exp_date=" + request.getParameter("x_exp_date") + "&";
		String passCardCode = "x_card_code=" + request.getParameter("x_card_code") + "&";
		String passTransId = "x_trans_id=" + request.getParameter("x_trans_id") + "&";
		String passAuthCode = "x_auth_code=" + request.getParameter("x_auth_code") + "&";

		StringBuffer sb = new StringBuffer();
		sb.append(passLogin);
		sb.append(passTranKey);
		sb.append(passVersion);
		sb.append(passTestReq);
		sb.append(passDupWindow);

		sb.append("x_delim_data=TRUE&");
		sb.append("x_delim_char=|&");
		sb.append("x_relay_response=FALSE&");

		// Transaction Information
		sb.append("x_method=CC&");
		sb.append(passTranType);
		sb.append(passAmount);

		String sbOut = sb.toString();

		// CC information
		sb.append(passCardNum);
		sb.append(passExpDate);
		sb.append(passCardCode);
		sb.append(passFirstName);
		sb.append(passLastName);
		sb.append(passAddress);
		sb.append(passCity);
		sb.append(passState);
		sb.append(passZip);

		sb.append(passCustId);
		
		String tranType = request.getParameter("x_type"); 
		if (tranType.equals("CREDIT") || tranType.equals("VOID") || tranType.equals("PRIOR_AUTH_CAPTURE")) {
			sb.append(passTransId);
		}

		if (tranType.equals("CAPTURE_ONLY")) {
			sb.append(passAuthCode);
		}

		sb.append("x_description=Java Transaction&");
		String line ="";
		// --------------------------------------------------------------------------------
		try {
			// Authorize.net TEST URL (Expires after 90 days)
			URL url = new URL(PaymentService.TEST_URL);

			// Authorize.net PROD URL
			// URL url = new
			// URL("https://secure2.authorize.net/gateway/transact.dll");
			// URL url = new
			// URL("http://192.168.50.38:8080/WbPrx/Pmt");//?"+sb);

			String last4dgtCardNum = (passCardNum != null && passCardNum.length() > 17)
					? passCardNum.substring(16, (passCardNum.length() - 1)) : "NoCardNum";
			System.out.println(" creditcard sbOut:  " + sbOut + " last4dgtCardNum: " + last4dgtCardNum
					+ " passAuthCode: " + passAuthCode + " passTransId: " + passTransId + " creditcard url :" + url
					+ " today: " + new java.util.Date());
			// Open Connection
			// URLConnection connection = url.openConnection();

			// SSL-Specific Feature URL

			HttpsURLConnection connection = (HttpsURLConnection) url.openConnection();
			connection.setHostnameVerifier(new HostnameVerifier() {
				public boolean verify(String urlHostname, SSLSession sslSession) {
					return true;
				}
			});

			connection.setDoOutput(true);
			connection.setUseCaches(false);

			connection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");

			// POST the data in the string buffer
			DataOutputStream out = new DataOutputStream(connection.getOutputStream());
			out.write(sb.toString().getBytes());
			out.flush();
			out.close();

			// Process and Read the Response
			BufferedReader in = new BufferedReader(new InputStreamReader(connection.getInputStream()));
			
			line = in.readLine();
			in.close();
			System.err.println(line);

			} catch (Exception e) {
			System.out.print("(CreditCard:" + new java.util.Date() + ") Error in CreditCard: " + e.toString());

			e.printStackTrace();
		}

		return  line;
	}

	public static String processCrCard(CreditCardType crCardType, String amount, String login, String tranKey,
			String itemId, boolean isProdEnvironment, Properties props, String transType, String passTransId,
			CustomerDataType customerDataType, CustomerAddressType customerAddressType, String passAuthCode) {

		Environment environment = Environment.SANDBOX;
		ApiOperationBase.setEnvironment(environment);
		String companyNum = "";
		MerchantAuthenticationType merchantAuthenticationType = new MerchantAuthenticationType();
		if (login == null || login.length() < 2) {
			login = "88gNkH23";
		}

		if (login.equals("88gNkH23"))
			companyNum = "1000";

		merchantAuthenticationType.setName(login);
		if (tranKey == null || tranKey.length() < 2)
			tranKey = "86qaYHUn392gvT7d";
		merchantAuthenticationType.setTransactionKey(tranKey);

		String nameOnAccount = customerAddressType.getFirstName() + " " + customerAddressType.getLastName();
		ApiOperationBase.setMerchantAuthentication(merchantAuthenticationType);

		PaymentType paymentType = new PaymentType();
		paymentType.setCreditCard(crCardType);

		TransactionRequestType txnRequest = new TransactionRequestType();

		if (transType.equals("AUTH_CAPTURE"))
			txnRequest.setTransactionType(TransactionTypeEnum.AUTH_CAPTURE_TRANSACTION.value());
		else if (transType.equals("AUTH_ONLY "))
			txnRequest.setTransactionType(TransactionTypeEnum.AUTH_ONLY_TRANSACTION.value());
		else if (transType.equals("PRIOR_AUTH_CAPTURE"))
			txnRequest.setTransactionType(TransactionTypeEnum.PRIOR_AUTH_CAPTURE_TRANSACTION.value());
		else if (transType.equals("CREDIT"))
			txnRequest.setTransactionType(TransactionTypeEnum.CAPTURE_ONLY_TRANSACTION.value()); 
		// ??? Could not find a correspondent transType
		else if (transType.equals("VOID"))
			txnRequest.setTransactionType(TransactionTypeEnum.VOID_TRANSACTION.value());
		else if (transType.equals("CAPTURE_ONLY")) 
			txnRequest.setTransactionType(TransactionTypeEnum.CAPTURE_ONLY_TRANSACTION.value());
			// no trans id here - passAuthCode instead
		else
			txnRequest.setTransactionType(transType);//
		// txnRequest.setTransactionType(TransactionTypeEnum.AUTH_CAPTURE_TRANSACTION.value());//transType);//
		txnRequest.setAuthCode(passAuthCode);
		txnRequest.setRefTransId(passTransId);
		txnRequest.setPayment(paymentType);
		// txnRequest.setCustomer(customerDataType);
		// txnRequest.setBillTo(customerAddressType);
		// if (amount == null || amount.length() < 2) amount = "54.24";

		BigDecimal amountBD = (new BigDecimal(amount)).setScale(2, RoundingMode.HALF_EVEN);
		txnRequest.setAmount(amountBD);

		// Make the API Request
		CreateTransactionRequest apiRequest = new CreateTransactionRequest();
		apiRequest.setTransactionRequest(txnRequest);
		CreateTransactionController controller = new CreateTransactionController(apiRequest);

		String paymentInfo = "  \nr Response Code: " + "  Readable Response Code: " + "  Approval Code: "
				+ "  Trans ID: " + "  MD5 Hash: ";
		String returnMessage = processCredCardTransactionResponse(controller, "Credit Card: ", itemId,
				isProdEnvironment, props, nameOnAccount, amountBD, companyNum, paymentInfo);
		returnMessage = returnMessage != null ? returnMessage : "";
		
		return returnMessage;

	}

	public static String processCredCardTransactionResponse(CreateTransactionController controller,
			String returnMessage, String itemId, boolean isProdEnvironment, java.util.Properties props,
			String payeeName, BigDecimal amountBD, String companyNumber, String paymentInfo) {

		StringBuffer resultMessageSB = new StringBuffer();
		String responseReasonText = "";
		if (isProdEnvironment) {
			resultMessageSB.append("This transaction ");
		} else {
			resultMessageSB.append(
					"Testing. Attention: This is a test transaction, it should not be processed!!!\r\n This transaction ");
		}
		TransactionResponse transactionResponse = null;
		MessagesType messagesType = null;
		boolean hasMessage = false;
		try {
			controller.execute();
			CreateTransactionResponse response = controller.getApiResponse();
			String amount = NumberFormat.getCurrencyInstance().format(amountBD);
			List<Message> messages = null;

			if (response != null) {
				messagesType = response.getMessages();
				if (messagesType != null && messagesType.getMessage() != null && messagesType.getMessage().size() > 0
						&& messagesType.getMessage().get(0) != null) {
					hasMessage = true;
					messages = messagesType.getMessage();
					responseReasonText = responseReasonText + "|messagesCode=" + messages.get(0).getCode()
							+ "|messagesText=" + messages.get(0).getText() + "|messagesResultCode="
							+ messagesType.getResultCode();
				}
			}

			if (response != null && response.getTransactionResponse() != null) {
				transactionResponse = response.getTransactionResponse();
				responseReasonText = responseReasonText + "|TransactionResponseResponseCode="
						+ transactionResponse.getResponseCode() + "|TransactionResponseAccountNumber="
						+ transactionResponse.getAccountNumber() + "|TransactionResponseAccountType="
						+ transactionResponse.getAccountType() + "|TransactionResponseAuthCode()="
						+ transactionResponse.getAuthCode() + "|transactionResponseTransHash="
						+ transactionResponse.getTransHash() + "|transactionResponseAvsResultCode="
						+ transactionResponse.getAvsResultCode() + "|transactionResponseCavvResultCode="
						+ transactionResponse.getCavvResultCode() + "|TransactionResponseTransId="
						+ transactionResponse.getTransId();

				if (transactionResponse.getResponseCode().equals("1")) {
					// 2 messagesType.getResultCode() == MessageTypeEnum.OK &&
					String dt = new SimpleDateFormat("MM/dd/yyyy").format(new java.util.Date());

					returnMessage = "Payment Date: " + dt + "  Payment Amount: " + amount + "  Transaction ID: "
							+ transactionResponse.getTransId();
					resultMessageSB.append(" with " + returnMessage + " was processed successfully. \r\n ");

				} else {
					resultMessageSB.insert(0, "Error: ");
					resultMessageSB.append(" failed: ");
					returnMessage = "Error: ";
					try {
						if (hasMessage) {
							for (Message message : messages) {
								if (message != null) {
									String errMsg = message.getText() != null ? message.getText() : "";

									resultMessageSB.append("\r\n Code:" + message.getCode() + " Message:" + errMsg);
									if (errMsg.equals(PaymentService.AUTH_ERR)) {
										returnMessage += PaymentService.AUTH_ERR_DISPL;
									}
								}
							}
						}
						List<TransactionResponse.Errors.Error> errors = transactionResponse != null
								&& transactionResponse.getErrors() != null ? transactionResponse.getErrors().getError()
										: null;
						if (errors != null) {
							for (TransactionResponse.Errors.Error error : errors) {
								if (error != null && error.getErrorText() != null) {
									String errText = error.getErrorText();
									resultMessageSB.append(
											"\r\n Error Code:" + error.getErrorCode() + "\r\n Error: " + errText);
									if (errText.equals(PaymentService.ACC_ERR)) {
										returnMessage += errText + ".";
									} else if (errText.equals(PaymentService.ROUT_ERR)) {
										returnMessage += PaymentService.ROUT_ERR_DISPL;
									} else if (errText.equals(PaymentService.DUPL_ERR)) {
										returnMessage += PaymentService.DUPL_ERR;
									} else if (errText.equals(PaymentService.DECLINED_ERR)) {
										returnMessage += PaymentService.DECLINED_ERR
												+ "\n\r Please try to change payment amount";
									} else if (errText.equals(PaymentService.NOCHECKS_ERR)) {
										returnMessage += PaymentService.NOCHECKS_ERR_DISPL;
									} else if (errText.equals(PaymentService.MAXEXCEEDED_ERR)) {
										returnMessage += PaymentService.MAXEXCEEDED_ERR
												+ "\n\r Please try to change payment amount";
									} else {
										returnMessage += PaymentService.GENERIC_ERR_DISPL;
									}
								} else {
									returnMessage += PaymentService.GENERIC_ERR_DISPL;
								}
							}
						}
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
			} else {
				returnMessage = "Error: " + PaymentService.GENERIC_ERR_DISPL;
				resultMessageSB.insert(0, "Error: SYSTEM ERROR \r\n");
				resultMessageSB.append(" failed: ");
				List<String> results = controller.getResults();
				if (results != null && results.size() > 0) {
					for (String result : results) {
						System.out.println("\r\nresult" + result);
						resultMessageSB.append(" failed: " + result);
					}
				}
			}
			if (returnMessage != null && returnMessage.equals("Error: "))
				returnMessage += PaymentService.GENERIC_ERR_DISPL;
			resultMessageSB.append(" \r\n Policy: " + itemId + " \r\n " + paymentInfo + "   \r\n Amount: " + amount
					+ "   \r\n Name on a check: " + payeeName);
		} catch (Exception e) {
			resultMessageSB.insert(0, "Error: SYSTEM ERROR \r\n");
			resultMessageSB.append(" failed: ");
			returnMessage = "Error: " + returnMessage;
			e.printStackTrace();
		}

	
		return responseReasonText + "|" + returnMessage;
	}

}
