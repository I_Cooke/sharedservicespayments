package com.bintech.webservices.payment;

import java.io.IOException;
import java.util.Hashtable;
import java.util.Properties;

import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * @author icooke
 * @Created On July 18, 2016
 * @Modified by icooke On July 18, 2016 Ticket "42 - Authorize.Net - Configure
 *           new Proxy Server"
 */
public class PaymentService extends HttpServlet {

	private static final long serialVersionUID = -1181412506177662356L;

	static final String ACC_ERR = "The account number is invalid";
	static final String ROUT_ERR = "The ABA code is invalid";
	static final String ROUT_ERR_DISPL = "The Bank Routing Number is invalid.";
	static final String AUTH_ERR = "User authentication failed due to invalid authentication values.";
	static final String AUTH_ERR_DISPL = " There is a problem with processing this transaction."
			+ "\r\nPlease contact support at 1-800-627-0000.";

	static final String GENERIC_ERR_DISPL = " eCheck Paymend failed, could not process this payment."
			+ "\r\nPlease contact support at 1-800-627-0000.";
	static final String DUPL_ERR = "A duplicate transaction has been submitted.";
	static final String DECLINED_ERR = "This transaction has been declined.";

	static final String NOCHECKS_ERR = "ACH transactions are not accepted by this merchant.";

	static final String NOCHECKS_ERR_DISPL = "E-Checks payments are not allowed for this policy."
			+ "\r\nPlease contact support at 1-800-627-0000.";
	static final String MAXEXCEEDED_ERR = "The transaction amount submitted was greater than the maximum amount allowed.";

	static final String DIRECTORY_NAME = "";
	static final String LOCAL_URL ="http://savpdmzwebwv01.webdom.bankersinsurance.com:8080/WbPrx/Pmt";
	static final String TEST_URL ="https://test.authorize.net/gateway/transact.dll";
	static final String PROD_URL ="https://secure2.authorize.net/gateway/transact.dll";
	// private static Log logger = LogFactory.getLog(ProcessPayment.class);
	private static final Hashtable<String, String> remoteHostsHT = new Hashtable<String, String>();

	private static void initRemoteHostsHT() {
		remoteHostsHT.put("192.168.50.42", "DEV");
		remoteHostsHT.put("10.20.10.140", "Irina's PC");
		remoteHostsHT.put("192.168.50.42", "SQA");
		remoteHostsHT.put("192.168.50.41", "CA");
		remoteHostsHT.put("10.30.100.14", "Production Portal");
		remoteHostsHT.put("172.19.11.2", "Calvin");
		remoteHostsHT.put("10.30.101.10", "Production Hobbes");
		remoteHostsHT.put("192.168.50.38", "Web Proxy Local 192 dev");
		remoteHostsHT.put("0:0:0:0:0:0:0:1", "Web Proxy Local 0:0");
		remoteHostsHT.put("127.0.0.1", "Web Proxy Local 127");
		remoteHostsHT.put("192.168.50.45", "Web Proxy Local 192 prod");
		remoteHostsHT.put("10.30.8.34", "Web Proxy Local 192 Calvin");
	}

	// * HTTP Get Method

	public void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		if (remoteHostsHT.size() == 0)
			PaymentService.initRemoteHostsHT();
		doPost(req, resp);
	}

	// * HTTP Post Method

	public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		if (remoteHostsHT.size() == 0)
			PaymentService.initRemoteHostsHT();
		
		java.util.Set<String> incomingIps = remoteHostsHT.keySet();
		Properties props = new Properties();
		String remoteHost = request.getRemoteHost() != null ? request.getRemoteHost() : "";
		String returnMessage = "";
		if (remoteHost.length() > 0 && incomingIps.contains(remoteHost)) {
			String remoteHostName = remoteHostsHT.get(remoteHost);
			System.out.println("\n Valid request from: " + remoteHost);
			if (remoteHostName.startsWith("Production"))
				props.load(new java.io.BufferedInputStream(new java.io.FileInputStream(
						this.getServletContext().getRealPath("/") + "/properties/payment.properties")));
			else
				props.load(new java.io.BufferedInputStream(new java.io.FileInputStream(
						this.getServletContext().getRealPath("/") + "/properties/test_payment.properties")));

		} else {
			returnMessage = "\n Invalid request from: " + remoteHostsHT.get(remoteHost);
		}
		if (returnMessage != null 
				&& returnMessage.length() == 0
				&& request.getParameter("pmtType")!=null
				&& request.getParameter("pmtType").length()>0) {
			String pmtType=request.getParameter("pmtType");
			
			if (pmtType.equals("CC")) { // Credit Card request
				returnMessage = ProcessCredCard.processCredCardRequest(request, response, props);
			} else if (pmtType.equals("ACH")) { // E-Check request
				try {
					returnMessage = ProcessECheck.processECheckRequest(request, response, props);
				} catch (Exception e) {
					returnMessage = "Error calling Credit Card Request" + e;
					System.out.println(returnMessage);
				}
			} else {
				returnMessage = "Invalid payment Request, no payment amount is specified.";
				System.out.println(returnMessage);
			}
		}
		System.out.println("\nreturnMessage="+returnMessage);
		ServletOutputStream out = response.getOutputStream();
		response.setContentType("text/html");
		out.println(returnMessage);
		out.close();

	}

}
