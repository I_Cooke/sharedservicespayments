package test.bintech.webservices.payment;

import java.math.BigDecimal;
import java.math.RoundingMode;

import com.bintech.webservices.payment.ProcessCredCard;

import net.authorize.Environment;
import net.authorize.api.contract.v1.BankAccountTypeEnum;
import net.authorize.api.contract.v1.CreateTransactionRequest;
import net.authorize.api.contract.v1.EcheckTypeEnum;
import net.authorize.api.contract.v1.MerchantAuthenticationType;
import net.authorize.api.contract.v1.PaymentType;
import net.authorize.api.contract.v1.TransactionRequestType;
import net.authorize.api.contract.v1.TransactionTypeEnum;
import net.authorize.api.controller.CreateTransactionController;
import net.authorize.api.controller.base.ApiOperationBase;

/**
 * @author icooke
 * @Created On May 12, 2016
 * @Modified by icooke On May 12, 2016 Ticket #82360
 */
public class TestPayments {
	
	public static void main(String[] args) {
		// C:\BIG_DEVELOPER_WORKAREA\Indigo_Workspace\BinTech\WebContent\WEB-INF>
		// java -cp "./lib/*;./classes"
		// com.bintech.webservices.payment.ProcessPayment
	
		String amount = "1.53";
		String id = "0";
		String tranKey = "0";
		if (args != null && args.length > 2) {
			amount = args[0];
			id = args[1];
			tranKey = args[2];
		}
		
		java.util.Properties props = new java.util.Properties();
		props.setProperty("3000ECheckInterfaceId","88gNkH23");
		props.setProperty("3000ECheckInterfaceKey","86qaYHUn392gvT7d");
		props.setProperty("eCheckFtpUser","echeck");
		props.setProperty("eCheckFtpPwd","Acc0unting@1");
		props.setProperty("eCheckFtpPort","21");		
		props.setProperty("eCheckFtpServer","BHPSHARESWH01");
	
		BigDecimal amountBD = (new BigDecimal(amount)).setScale(2, RoundingMode.HALF_EVEN);
		CreateTransactionController controller = null;
		
		/*controller = TestPayments.processTestECheck(amount, id, tranKey);
		String returnMessage = ProcessPayment.processTransactionResponse(
				controller, "", "testPolicy", false, props,  "testCustId", amountBD,  "3000","testing");
			
		System.out.println("\n processTestECheck E-Check message:" + returnMessage);*/
		controller = TestPayments.processTestCrCard(amount, id, tranKey);
		String returnMessage = ProcessCredCard.processCredCardTransactionResponse(controller,  " Credit Card:", "testPolicy", false
				, props, "testCustId", amountBD,  "3000","testing");
		System.out.println("\n processTestCrCard Credit Card message:" + returnMessage);
		//TestPayments.processTestRPGCrCard();
		
		/*TestPayments.testProdCrCard(); 
		
		controller = TestPayments.testProdECheck();
		returnMessage = processTransactionResponse(controller, "", "09-0011429607-2-01", true, "090011429607201",
				null, "testProdECheckP", amountBD, "3000", "testingInProd");
		System.out.println("\n testProdECheck E-Check message:" + returnMessage);
		*/
	}


	public static CreateTransactionController processTestCrCard(String amount, String id, String key) {

		Environment environment = Environment.SANDBOX;
		ApiOperationBase.setEnvironment(environment);

		MerchantAuthenticationType merchantAuthenticationType = new MerchantAuthenticationType();
		if (id == null || id.length() < 2)
			id = "88gNkH23";
		merchantAuthenticationType.setName(id);
		if (key == null || key.length() < 2)
			key = "86qaYHUn392gvT7d";
		merchantAuthenticationType.setTransactionKey(key);
		ApiOperationBase.setMerchantAuthentication(merchantAuthenticationType);

		PaymentType paymentType = new PaymentType();

		net.authorize.api.contract.v1.CreditCardType crCardType = new net.authorize.api.contract.v1.CreditCardType();
		crCardType.setCardCode("998");
		crCardType.setCardNumber("5424000000000015");
		crCardType.setExpirationDate("2023-12");
		paymentType.setCreditCard(crCardType);

		TransactionRequestType txnRequest = new TransactionRequestType();
		txnRequest.setTransactionType(TransactionTypeEnum.AUTH_CAPTURE_TRANSACTION.value());
		txnRequest.setPayment(paymentType);
		// txnRequest.setCustomer(customer);
		if (amount == null || amount.length() < 2)
			amount = "54.24";

		BigDecimal amountBD = (new BigDecimal(amount)).setScale(2, RoundingMode.HALF_EVEN);
		txnRequest.setAmount(amountBD);

		// Make the API Request
		CreateTransactionRequest apiRequest = new CreateTransactionRequest();
		apiRequest.setTransactionRequest(txnRequest);
		CreateTransactionController controller = new CreateTransactionController(apiRequest);
		return controller;
		// System.out.println("\n CrCard message:" + returnMessage);
	}

	public static CreateTransactionController processTestECheck(String amount, String id, String key) {

		Environment environment = Environment.SANDBOX;
		ApiOperationBase.setEnvironment(environment);
		String payeeName = "John ECheck";
		MerchantAuthenticationType merchantAuthenticationType = new MerchantAuthenticationType();
		if (id == null || id.length() < 2)
			id = "88gNkH23";
		merchantAuthenticationType.setName(id);
		merchantAuthenticationType.setTransactionKey("86qaYHUn392gvT7d");
		ApiOperationBase.setMerchantAuthentication(merchantAuthenticationType);
		PaymentType paymentType = new PaymentType();
		net.authorize.api.contract.v1.BankAccountType bankAccountType = new net.authorize.api.contract.v1.BankAccountType();

		bankAccountType.setAccountNumber("123456");

		bankAccountType.setAccountType(BankAccountTypeEnum.CHECKING);
		bankAccountType.setBankName("Bank of America");
		bankAccountType.setCheckNumber("123");
		bankAccountType.setEcheckType(EcheckTypeEnum.WEB);
		bankAccountType.setNameOnAccount(payeeName);
		bankAccountType.setRoutingNumber("063100277");
		paymentType.setBankAccount(bankAccountType);

		TransactionRequestType txnRequest = new TransactionRequestType();
		txnRequest.setTransactionType(TransactionTypeEnum.AUTH_CAPTURE_TRANSACTION.value());
		txnRequest.setPayment(paymentType);
		// txnRequest.setCustomer(customer);
		if (amount == null || amount.length() < 2)
			amount = "54.32";

		BigDecimal amountBD = (new BigDecimal(amount)).setScale(2, RoundingMode.HALF_EVEN);
		txnRequest.setAmount(amountBD);

		// Make the API Request
		CreateTransactionRequest apiRequest = new CreateTransactionRequest();
		apiRequest.setTransactionRequest(txnRequest);
		CreateTransactionController controller = new CreateTransactionController(apiRequest);
		return controller;
	}

	public static CreateTransactionController testProdECheck() {

		ApiOperationBase.setEnvironment(Environment.PRODUCTION);
		String payeeName = "John ECheck";
		MerchantAuthenticationType merchantAuthenticationType = new MerchantAuthenticationType();

		String id = "66rFpF2pr";
		merchantAuthenticationType.setName(id);
		merchantAuthenticationType.setTransactionKey("6P6R8K62r3e3m88q");
		ApiOperationBase.setMerchantAuthentication(merchantAuthenticationType);
		PaymentType paymentType = new PaymentType();
		net.authorize.api.contract.v1.BankAccountType bankAccountType = new net.authorize.api.contract.v1.BankAccountType();

		bankAccountType.setAccountNumber("12345699");

		bankAccountType.setAccountType(BankAccountTypeEnum.CHECKING);
		bankAccountType.setBankName("Bank of America");
		bankAccountType.setCheckNumber("123");
		bankAccountType.setEcheckType(EcheckTypeEnum.WEB);
		bankAccountType.setNameOnAccount(payeeName);
		bankAccountType.setRoutingNumber("063100277");
		paymentType.setBankAccount(bankAccountType);

		TransactionRequestType txnRequest = new TransactionRequestType();
		txnRequest.setTransactionType(TransactionTypeEnum.AUTH_CAPTURE_TRANSACTION.value());
		txnRequest.setPayment(paymentType);
		// txnRequest.setCustomer(customer);

		String amount = "1.32";

		BigDecimal amountBD = (new BigDecimal(amount)).setScale(2, RoundingMode.HALF_EVEN);
		txnRequest.setAmount(amountBD);

		// Make the API Request
		CreateTransactionRequest apiRequest = new CreateTransactionRequest();
		apiRequest.setTransactionRequest(txnRequest);
		CreateTransactionController controller = new CreateTransactionController(apiRequest);
		return controller;
	}
}
